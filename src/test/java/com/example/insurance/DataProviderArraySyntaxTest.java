package com.example.insurance;

import com.example.insurance.pojo.Driver;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;
import java.io.FileReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

@RunWith(DataProviderRunner.class)
public class DataProviderArraySyntaxTest {
    private static final String DRIVERS_FILE = "src\\test\\java\\resources\\Drivers.json";

    @Autowired
    private JsonService jsonService;


    @DataProvider
    public static Object[][] dataProviderAdd() {
        return new Object[][]{
                {0, 0, 0},
                {1, 1, 2},
                /* ... */
        };
    }

    @DataProvider
    public static Object[][] drivers() throws Exception{
        List<Driver> drivers = getJsonService().fromJsonList(Files.readString(new File(DRIVERS_FILE).toPath(), StandardCharsets.UTF_8), Driver.class);

        Object[][] objects = new Object[drivers.size()][1];

        int index = 0;
        for (Object[] obj : objects) {
            obj[0]=drivers.get(index);
        }

        return objects;
    }

    private static JsonService getJsonService(){
        return new JsonService(objectMapper());
    }

    public static ObjectMapper objectMapper() {
        ObjectMapper mapper = new ObjectMapper();

        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        mapper.configure(MapperFeature.DEFAULT_VIEW_INCLUSION, true);
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.registerModule(new JavaTimeModule());

        return mapper;
    }

    @Test
    @UseDataProvider("drivers")
    public void testDrivers(Driver driver) {
        System.out.println(driver);
    }


    @Test
    @UseDataProvider("dataProviderAdd")
    public void testAdd(int a, int b, int expected) {
        // When:
        int result = a + b;

        // Then:
        Assert.assertEquals(expected, result);
    }
}
