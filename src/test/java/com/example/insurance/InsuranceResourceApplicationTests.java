package com.example.insurance;

import com.example.insurance.domain.Insurance;
import com.example.insurance.service.InsuranceService;
import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.time.LocalDateTime;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureMockMvc
@RequiredArgsConstructor
class InsuranceResourceApplicationTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private JsonService jsonService;


    @Test
    void addNewInsurance() throws Exception {
        InsuranceService.NewInsuranceRequest newInsuranceRequest = InsuranceService.NewInsuranceRequest.builder()
                .startDate(LocalDateTime.now().toString())
                .endDate(LocalDateTime.now().toString())
                .age(14d)
                .region("Moscow")
                .city("Moscow")
                .insurance(Insurance.builder()
                        .id(12)
                        .startDate(LocalDateTime.now().toString())
                        .endDate(LocalDateTime.now().toString())
                        .build()
                ).build();


        mockMvc.perform(post("/insurance/new")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonService.toJson(newInsuranceRequest))
        )
                .andDo(print())
                .andExpect(status().isOk());
    }


    @Test
    void getInsuranceByUser() throws Exception {
        mockMvc.perform(get("/insurance/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1));
    }


    @Test
    void getInsuranceByUserWithNotValid() throws Exception {
        mockMvc.perform(get("/insurance/404"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(404));
    }

    @Test
    void checkSave() throws Exception {
        double age = 15;

        MvcResult mvcResult = mockMvc.perform(post("/insurance/new")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonService.toJson(getNewInsurance(age)))
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        String insuranceId = mvcResult.getResponse().getContentAsString();

        mockMvc.perform(get("/insurance/".concat(insuranceId)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(age));
    }

    @Test
    void invalidAge() throws Exception {
        String age = "foo";

        NewInsuranceAgeStringRequest request = NewInsuranceAgeStringRequest.builder()
                .startDate(LocalDateTime.now().toString())
                .endDate(LocalDateTime.now().toString())
                .age(age)
                .region("Moscow")
                .city("Moscow")
                .insurance(Insurance.builder()
                        .startDate(LocalDateTime.now().toString())
                        .endDate(LocalDateTime.now().toString())
                        .build()
                ).build();

        mockMvc.perform(post("/insurance/new")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonService.toJson(request))
        )
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    void vasya() throws Exception {
        String age = "vasya";
        mockMvc.perform(get("/name/" + age))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.age").value(15));
    }

    private InsuranceService.NewInsuranceRequest getNewInsurance(Double age) {
        return InsuranceService.NewInsuranceRequest.builder()
                .startDate(LocalDateTime.now().toString())
                .endDate(LocalDateTime.now().toString())
                .age(age)
                .region("Moscow")
                .city("Moscow")
                .insurance(Insurance.builder()
                        .startDate(LocalDateTime.now().toString())
                        .endDate(LocalDateTime.now().toString())
                        .build()
                ).build();
    }


    @Builder
    @Getter
    public static class NewInsuranceAgeStringRequest {
        private String startDate;
        private String endDate;
        private String age;
        private String region;
        private String city;
        private Insurance insurance;
    }


    @DataProvider
    public static Object[][] sumTestData() {
        return new Object[][]{
                {2, 2, 4},
                {10, 1, 11},
                {1000000, -1000000, 0}
        };
    }

    @Test
    @UseDataProvider("sumTestData")
    public void dataProviderTest(int a, int b, int expectedSum) {
        Assert.assertEquals(expectedSum, a + b);
    }
}
