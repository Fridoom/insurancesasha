package com.example.insurance.pojo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.junit.Ignore;

import java.util.ArrayList;
import java.util.List;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Driver {
    private String testName;
    private String calcKey;
    private List<String> driverKeyList;
    private Boolean driversRestriction;
    private Boolean personDocumentAdd;
    private Boolean followToRegistration;


    public static Driver create(){
        ArrayList<String> objects = new ArrayList<>();
        objects.add("Ferst");
        objects.add("trrrot");
        return Driver.builder()
                .testName("testName")
                .calcKey("calcKey")
                .driverKeyList(objects)
                .driversRestriction(false)
                .personDocumentAdd(false)
                .followToRegistration(true)
                .build();
    }

}
