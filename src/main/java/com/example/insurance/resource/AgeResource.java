package com.example.insurance.resource;

import com.example.insurance.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/name/")
@RequiredArgsConstructor
public class AgeResource {

    private final UserService userService;

    @GetMapping("/{name}")
    public UserService.ReturnAgeWithName getAge(@PathVariable String name){
        return userService.getAge(name);
    }

//    @GetMapping("/f")
//    @ResponseBody
//    public UserService.ReturnAgeWithName getAge(@RequestParam String name){
//        return userService.getAge(name);
//    }
}
