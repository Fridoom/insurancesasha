package com.example.insurance.domain;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Getter
@AllArgsConstructor
@Builder
public class User {

    private String name;
    private String age;

    public static User returnWithName(String name){
        return User.builder().name(name).build();
    }

}
