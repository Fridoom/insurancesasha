package com.example.insurance.service;

import com.example.insurance.domain.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class UserService {

    public ReturnAgeWithName getAge(String name){

        if(name.equals("vasya")){
            return ReturnAgeWithName.builder().name(name).age("15").build();
        }else {
            return ReturnAgeWithName.builder().name(name).age("100").build();
        }
    }


    public String withName(ReturnAgeWithName request){
        if(request.name == "vasya"){
            request.age = "15";
        }else {
            request.age = "100";
        }
        return request.age;
    }

    @Getter
    @Builder
    public static class ReturnAgeWithName {
        public String name;
        public String age;
    }
}
